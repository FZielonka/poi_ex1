from scipy.stats import norm
from scipy.stats import uniform
from csv import writer
import numpy as np


def gen_horizontal_surface(width, length, num_points=2000):
    distribution_x = norm(loc=0, scale=width/6)  # dzielenie przez 6 aby 99.73% punktów mieściło się w zadanym wymiarze
    distribution_y = norm(loc=0, scale=length/6)
    distribution_z = norm(loc=-70, scale=0.05)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)
    points = zip(x, y, z)
    return points


def gen_vertical_surface(width, height, num_points=2000):
    distribution_x = norm(loc=0, scale=width/6)
    distribution_y = norm(loc=0, scale=0.05)
    distribution_z = norm(loc=0.2, scale=height/6)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)
    points = zip(x, y, z)
    return points


def gen_cylindrical_surface(radius, height, num_points=2000):
    distribution_rho = norm(loc=radius, scale=0.01)
    distribution_phi = uniform(loc=0, scale=2*np.pi)
    distribution_z = norm(loc=0, scale=height/3)

    rho = distribution_rho.rvs(size=num_points)
    phi = distribution_phi.rvs(size=num_points)*np.sin(distribution_phi.rvs(size=num_points))
    z = distribution_z.rvs(size=num_points)

    x=[]
    y=[]
    for i in range(num_points):
        x.append(rho[i] * np.cos(phi[i]))
        y.append(rho[i] * np.sin(phi[i]))
    points = zip(x,y,z)
    return points


def write_cloud_to_file(points, filename):
    with open(filename, 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)

        for p in points:
            csvwriter.writerow(p)
        csvfile.close()


if __name__ == '__main__':
    point_cloud = []

    hor_surface = gen_horizontal_surface(100, 150)
    point_cloud.extend(hor_surface)
    write_cloud_to_file(hor_surface, "horizontal_surface.xyz")

    ver_surface = gen_vertical_surface(100, 150)
    point_cloud.extend(ver_surface)
    write_cloud_to_file(ver_surface, "vertical_surface.xyz")

    cyl_surface = gen_cylindrical_surface(100, 80)
    point_cloud.extend(cyl_surface)
    write_cloud_to_file(cyl_surface, "cylindrical_surface.xyz")

    write_cloud_to_file(point_cloud, "point_cloud.xyz")

